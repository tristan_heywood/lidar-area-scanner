import serial
import queue
import threading
import time
from random import randrange
import cloud
import sys
import pdb
from math import *


def start_display(disp) :
	disp.display()

comPort = 'COM4'
if len(sys.argv) >= 2 :
	comPort = 'COM' + sys.argv[1]

print(comPort)
plotQ = queue.Queue() #queue of tuples of the form (x, y, z)
bOutQ = queue.Queue()
inputQ = queue.Queue()
consoleQ = queue.Queue()

disp = cloud.Display(plotQ, inputQ, consoleQ)

plotQ.put((randrange(-30, 30), randrange(-30, 30), randrange(100, 105)))
thr = threading.Thread(target = start_display, kwargs = {"disp" : disp})

thr.daemon = True
thr.start()

# while True :
# 	time.sleep(0.01)
# 	plotQ.put((randrange(-30, 30), randrange(-30, 30), randrange(100, 500)))

ser = serial.Serial(
	port = comPort,
	baudrate=19200,
	parity = serial.PARITY_ODD,
	stopbits = serial.STOPBITS_TWO,
	bytesize = serial.SEVENBITS)
print(ser.name)



def userInput() :
	while True :
		inStr = input()
		if inStr[0] == 'q' :
			sys.exit(0)
		inputQ.put(inStr)

inthr = threading.Thread(target = userInput)
inthr.daemon = True
inthr.start()

recv = b''
recvStr = ''

while True :
	if not inputQ.empty() > 0 :
		toSend = inputQ.get().encode('ascii')
		print("SENDING: [" + str(toSend) + ']')
		consoleQ.put("SENDING: [" + str(toSend) + ']')
		ser.write(toSend)

	if ser.inWaiting() > 0 :
		recv = ser.read(ser.inWaiting())
		recvStr += str(recv)[2:-1]
		if '_' in recvStr :
			token, recvStr = recvStr.split('_', 1)
			print("RECIEVED: [" + token + "]")
			consoleQ.put('RECIEVED: [' + token + ']')
			if token.startswith('P('):
				try :
					nums = token[token.find('(')+1 : token.find(')')]
					#plotQ.put(tuple([int(x.strip()) for x in nums.split(',')]))

					#UNTESTED
					pan, tilt, dist = [float(x.strip()) for x in nums.split(',')]
				except:
					pass
				xCo = int((dist / 1000 * sin(radians(tilt  / 100))) * 100)
				zCo = int((dist / 1000) * cos(radians(tilt  / 100)) * 100)
				yCo = int((dist / 1000) * sin(radians(pan / 100)) * 100)
				#print('adding to plotQ: ', xCo, yCo, zCo);
				plotQ.put((xCo, yCo, zCo))

	# if ser.inWaiting() > 0 :
	# 	recv += ser.read(ser.inWaiting())
	# 	# ind = recv.find(b'\x01')
	# 	# while ind != -1 and ind + 2 < len(recv) : #if we have recieved the unsigned int character, and there are two characters after it
	# 	# 	recv = recv[0:ind] + bytes(str(recv[ind + 1] * 0x0100 + recv[ind + 2]), 'ascii') + recv[ind + 3:] #get value of higher bits and shift
	# 	# 	ind = recv.find(b'\x01')
	# 	print("RECIEVED: [" + repr(recv) + "]") #use repr to attempt to display non pritables
	# 	recv = recv[-75:]
	time.sleep(0.001) #could break things - to stop my laptop fan from turning on doing useless work


#
# buf = ""
# #use _ as a new line character
# while True :
# 	if(ser.inWaiting() > 0) :
# 		buf += str(ser.read(ser.inWaiting()))
# 		print(buf)
# 		buf = ""
# 	# if "_" in str(buf) :
# 	# 	print(buf)
# 	# 	buf = ""
#
#
ser.write('abc'.encode())

serInput = ser.read(1)
print(serInput)


ser.write(b'b')

serInput = ser.read(1)
print(serInput)



ser.write(b'c')

serInput = ser.read(1)
print(serInput)



ser.close()
#
# ser.isOpen()
#
# while ser.inWaiting() > 0 :
#     out = ser.readline()
#     print out
