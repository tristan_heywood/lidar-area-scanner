import sys
import re
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

if len(sys.argv) > 1 :
    with open(sys.argv[1], 'r') as log :
        xList = []
        yList = []
        zList = []
        for line in log :
            x, y, z = [int(x) for x in line.split(',')]
            xList.append(x)
            yList.append(y)
            zList.append(z)
    print(xList, yList, zList)

fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.scatter(yList, zList, xList, c  = zList, marker = '.', cmap = matplotlib.cm.jet)
plt.show()
