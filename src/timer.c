void initTimer() {
    _asm CLI;      // Enable interrupts
    TSCR1 = 0x90 ; // Enable fast flag clear and normal timer operation
    TSCR2 = 0x04 ; // Set the prescaler to 16.
    TIOS  = (TIOS_IOS7_MASK | TIOS_IOS2_MASK) ; // ICs are low, OCs high.
    TCTL3 = 0x00 ; // Want to initiall capture time signal goes high and drops back on pin 1 (laser)
    TCTL4 = 0x04 ; // "
    TIE   = (TIE_C7I_MASK | TIE_C2I_MASK | TIE_C1I_MASK) ; // Enable interrupts

}

/* TIMER ALLOC  IC/OC   itr locations *
0 ...
1 Laser         IC      laser.c
2 Scan          OC      scan.c
3 ...
4 ...
5 ...
6 ...
7 Eduardo				OC			iic.c
*/
