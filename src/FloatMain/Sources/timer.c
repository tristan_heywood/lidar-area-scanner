/*!
   \file timer.c
   \brief Unified timer setup and system clock implementation.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong.
   \date 6 June 2017
*/

#include "derivative.h"
#include "timer.h"
#include "main.h"

volatile unsigned long int sysTime;
//!< Elapsed timer ticks since initTimer(), rounded down to nearest multiple of 2^16.

/* TIMER ALLOC  IC/OC   itr locations *
0 ...
1 Laser         IC      laser.c
2 ... Previously used by scan
3 7 seg display OC      segdisplay.c
4 ...
5 ...
6 ...
7 Eduardo				OC			iic.c
*/

/*!
   \brief Initialise timers with correct settings and enable necessary interrupts.
   \param "Param description"
   \pre "Pre-conditions"
   \post "Post-conditions"
   \return "Return of the function"
*/
void initTimer() {
    _asm SEI; //disable interrupts
    TSCR1 = 0x80 ; // normal timer operation, no fast flag clear
    TSCR2 = 0x84; // Set the prescaler to 16, enable overflow interrupts
    TIOS  = (TIOS_IOS7_MASK | TIOS_IOS3_MASK /*| TIOS_IOS2_MASK*/) ; // ICs are low, OCs high.
    TCTL3 = 0x00 ; // Want to initiall capture time signal goes high and drops back on pin 1 (laser)
    TCTL4 = 0x04 ; // "
    TIE   = (TIE_C1I_MASK | TIE_C3I_MASK /* | TIE_C2I_MASK*/) ; // Enable interrupts
    sysTime = 0; //initialise clock to zero
    _asm CLI;      // Enable interrupts
}

/*!
   \brief Gets elapsed timer ticks since initTimer() called, defined as system time.
	 Increments by 1500 per second, overflows after ~47 minutes.
   \return Number of timer ticks since initTimer().
*/
unsigned long int getSysTime() {
    volatile unsigned long int timer;
    volatile unsigned long int toRet;
    timer = TCNT;							// We add TCNT here to avoid wasting cycles with
    toRet = timer + sysTime; 	// every increment - add it when needed instead.
    return toRet;
}

/*!
   \brief Wrapper for do_later in main.c; used to add work to the queue by specifying
	 a delay before which the task executes.
   \param millis Delay before the function should be called.
	 \param func Function pointer to be placed on work queue.
	 \param data Pointer to be passed as argument to function when called on work queue.
   \pre initTimer() should have been called.
*/
void do_after(unsigned int millis, void (*func)(void*), void * data) {
    volatile unsigned long int delay;
    volatile unsigned long int syst;
    syst = getSysTime();
    delay = syst + (unsigned long int)millis * 1500UL; //with a prescale of 16, 1 ms = 1500 clock ticks
    call_do_later_macro(func, data, delay);
}

/*!
   \brief Interrupt service routine for the timer overflow interrupt, to increment
	 system clock by 2^16.
   \post sysTime incremented by 0xFFFF.
*/
interrupt 16 void tcnt_overflow_isr(void) {
    TFLG2 = 0x80; //write to bit 7 of TFLG2 to clear TOF (overflow flag)
    sysTime += 0xffffU;
    //with a prescaler of 16, each clock tick is 0.666... microseconds, or 16 cycles, and the clock will overflow after about 47 minutes
}
