#ifndef LCD_H
#define LCD_H

#define LCD_DATA PORTK
#define LCD_CTRL PORTK
#define RS 0x01
#define EN 0x02


void LCD_init();
void LCD_put_string(char * string);
char * get_LCD_buf_ptr(); 

void COMWRT4(unsigned char);
void DATWRT4(unsigned char);
void MSDelay(unsigned int);

#endif //LCD_H
