/*!
   \file servo.c
   \brief Servo motor control routines.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong.
   \date 6 June 2017
*/

#include "servo.h"
#include <hidef.h>
#include "derivative.h"
#include "lcd.h"
#include "timer.h"
#include <stdlib.h>
#include <stdio.h>
#include "serial.h"

#define PAN_CORRECTION 16						//!< Pan correction factor * 10 (must be integer).
#define TILT_CORRECTION 16					//!< Tilt correction factor * 10 (must be integer).
#define TILT_CORRECTION_ADD (-775)	//!< Tilt correction value, added to tilt input (integer centidegrees).

int panAngle;			//!< Last received pan command angle, centidegrees.
int tiltAngle;		//!< Last received tilt command angle, centidegrees.
char dispAngles;	//!< High if LCD should be updated with angles at 1Hz, else low.

/*!
   \brief Initialises PWM output for servo control, zeroes angles and variables.
   \pre Board should be connected to PTU and power applied.
   \post Servo motors ready for use, pointing to 0,0.
*/
void initServo() {
	PWMCTL |= 0xC0;						// Concatenate PWM (4&5) and (5&7), do not stop PWM in wait or freeze
	PWMPRCLK = 0x33;					// Clock A and B both prescaled by 2^3=8 (3MHz, or 60000 clocks per PWM output wavelength)
	PWMCLK = 0x00;						// Use clock A and B
	PWMPER45 = PERIOD_CYCLES;	// Period set to 20ms
	PWMPER67 = PERIOD_CYCLES;	// "
	PWMPOL = 0xFF;						// Start high (normal behaviour)
	PWMCAE = 0x00;						// Left align output
	PAN_PWM = CENTER;					// Center ptu
	TILT_PWM =	CENTER;				// "
	PTP = 0xF0;
	PWME = 0xF0;			// Turn on pwm output
	panAngle = 0;
	tiltAngle = 0;
	stop_disp_servo_LCD();
}

/*!
   \brief Checks whether the new setting for the PWM control registers are safe
	 (ie won't drive the servos outside of their allowable limits).
   \param value The value intended to be set on the PWM duty cycle register.
   \return High if safe, low if unsafe.
*/
unsigned char pwmSafe(unsigned int value) {
	return (value >= SAFE_MIN && value <= SAFE_MAX) ? 1 : 0; // If in range return 1, else 0
}

/*!
   \brief Converts a PWM duty cycle register value into a signed integer,
	 representing the deflection of the servo in centidegrees.
   \param pwm The PWM duty cycle register value.
   \return Signed deflection of servo motor in centidegrees.
*/
signed int angleFrom(unsigned int pwm) {
	return (5*pwm - 22500);
}

/*!
   \brief Converts a signed angle into a value appropriate for a PWM duty cycle register.
   \param angle Angle desired for servo motor.
   \return PWM duty cycle register value corresponding to the input angle.
*/
unsigned int pwmFrom(int angle) {
	return ((angle + 22500) / 5);
}

/*!
   \brief Sets the pan angle of the PTU.
   \param centidegrees Angle desired, expressed in centidegrees.
   \pre Servo control must be initialised.
   \post Pan angle of PTU head changed.
*/
void setPanAngle(int centiDegrees) {
		int result;
		result = centiDegrees / 10;
		result = result * PAN_CORRECTION;
		panAngle = centiDegrees;
		setPanPWM(pwmFrom(result));
}

/*!
   \brief Sets the tilt angle of the PTU.
   \param centidegrees Angle desired, expressed in centidegrees.
   \pre Servo control must be initialised.
   \post Tilt angle of PTU head changed.
*/
void setTiltAngle(int centiDegrees) {
		int result;
		result = centiDegrees / 10;
		result = result * TILT_CORRECTION + TILT_CORRECTION_ADD;
		tiltAngle = centiDegrees;
		setTiltPWM(pwmFrom(result));
}

/*!
   \brief Getter method for current pan angle.
   \return Current pan angle set, in signed centidegrees.
	 \note The servo motor takes time to move; the result of this method is not
	 guaranteed to be the orientation of the PTU if called within 500ms of a
	 setPanAngle() call.
*/
signed int getPanAngle() {
	return panAngle;
}

/*!
   \brief Getter method for current tilt angle.
   \return Current tilt angle set, in signed centidegrees.
	 \note The servo motor takes time to move; the result of this method is not
	 guaranteed to be the orientation of the PTU if called within 500ms of a
	 setTiltAngle() call.
*/
signed int getTiltAngle() {
	return tiltAngle;
}

/*!
   \brief Getter method for pan PWM duty cycle register.
   \return Current value of PWM duty cycle register for pan servo (uint16_t).
*/
unsigned int getPanPWM() {
	return PAN_PWM;
}

/*!
   \brief Getter method for tilt PWM duty cycle register.
   \return Current value of PWM duty cycle register for tilt servo (uint16_t).
*/
unsigned int getTiltPWM() {
	return TILT_PWM;
}

/*!
   \brief Setter method for pan PWM duty cycle register, checking safety.
   \param value PWM duty cycle value to set to the the pan servo.
   \pre Servo PWM should be initialised.
   \post Pan angle of PTU changed.
*/
void setPanPWM(unsigned int value) {
	if (pwmSafe(value)) {
		PAN_PWM = value;
	}
	return;
}

/*!
   \brief Setter method for tilt PWM duty cycle register, checking safety.
   \param value PWM duty cycle value to set to the the tilt servo.
   \pre Servo PWM should be initialised.
   \post Tilt angle of PTU changed.
*/
void setTiltPWM(unsigned int value) {
	if (pwmSafe(value)) {
		TILT_PWM = value;
	}
	return;
}

/*!
   \brief Enable regular printing of PTU orientation to LCD screen.
   \post LCD text changed.
*/
void start_disp_servo_LCD() {
		dispAngles = 1;
		disp_servo_LCD();
}

/*!
   \brief Disables regular printing of PTU orientation to LCD screen.
*/
void stop_disp_servo_LCD() {
		dispAngles = 0;
}

/*!
   \brief Prints PTU orientation of PTU to LCD screen.
   \param ptr For compatibility, pass a null pointer.
   \post LCD text changed.
	 \post If dispAngles high, another call rescheduled for 1000ms later.
*/
void disp_servo_LCD(void * ptr) {
		volatile float pan;
		volatile float tilt;
		pan = (float)getPanAngle() / 100;
		tilt = (float)getTiltAngle() / 100;
	  sprintf(get_LCD_buf_ptr(), "Azi: %+3.1f deg   Ele: %+3.1f deg", pan, tilt );

	  LCD_put_string(get_LCD_buf_ptr());
		if(dispAngles) {
				do_after(1000, disp_servo_LCD, NULL);
		}
}

/*!
   \brief Sends azimuth and elevation information over serial to PC.
   \param ptr For compatibility, pass a null pointer.
   \pre Serial must be initialised.
   \post String sent over serial.
*/
void send_az_el_to_PC(void * ptr) {
		sprintf(get_serial_buf_ptr(), "AE(%3.1f, %3.1f)", (float)getPanAngle() / 100, (float)getTiltAngle() / 100);
		serial_tx_str_vp((void*)get_serial_buf_ptr());
		do_after(1000, send_az_el_to_PC, NULL);
}
