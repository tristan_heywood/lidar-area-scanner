#ifndef SERIAL_H
#define SERIAL_H
#include "newScan.h"
#include "config.h"
#include <stdlib.h>
#include "polyscan.h"


#define BUFFER_LENGTH 100
#define WORK_QUEUE_LENGTH 30

#define ONE_RADIAN_IN_DEGREES 57.30

#define TRUE 1
#define FALSE 0


#define INC(bufPtr, buffer) do {bufPtr++; \
    if(bufPtr >= buffer + BUFFER_LENGTH) bufPtr = buffer; \
    } while(0)

char * get_serial_buf_ptr();
void serial_printf(const char * format, ...);
void angles_range_over_serial();
// void serial_tx_str_vp_free(void * voidP);
void C_sysTime_to_LCD(void*);
void get_angle_from_north(void *);
void heading_to_LCD(void *);
void elevation_to_LCD(void *); 
void set_magno_zero();
void c_sysTime_over_serial(void*);
void serial_tx_str_vp(void*);
void init_command(void);
void do_input_command(void * ptr);
void enter_serial_config();
void exit_serial_config();

#endif //SERIAL_H
