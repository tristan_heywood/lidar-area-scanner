/*!
   \file laser.c
   \brief Implementation of laser/LIDAR rangefinder control and distance calculations.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong
   \date 6 June 2017
*/

#include "laser.h"
#include "derivative.h"
#include "segdisplay.h"
#include <stdio.h>
#include "serial.h"

#define LASER_CORRECTION (-100) //!< Correction factor to add to each distance measurement.

unsigned int distance; //!< Store for the calculated distance.
unsigned int laserFreqDivisor; //!< Setting for how frequently distance should be updated.
unsigned int laserCounter; //!< Sample counter.
unsigned int prevAve;	//!< Store for averaging distance values.
int numLaserSamples; //!< Setting for number of samples to average over.

/*!
   \brief Setter method for samples per orientation.
   \param i The number of samples per orientation.
   \post Number of samples per orientation changed.
*/
void laser_set_samples(int i) {
    numLaserSamples = i;
}

/*!
   \brief interrupt subroutine implementing PWM timing and distance calculation.
   \pre This interrupt will only work correctly when timers have been configured
	 correctly (see timer.c) and LIDAR unit is turned on and functioning correctly.
   \post Distance may be updated, depending on settings.
*/
interrupt 9 void Laser_PWM_isr(void)
{
  static unsigned int rising_edge;
  static unsigned int falling_edge;
  TFLG1 |= TFLG1_C1F_MASK; // Clear interrupt flag for timer 1

	if(TCTL4 & 0x04) { //If detected a rising edge...
      rising_edge = TC1 ; // store its time stamp
      TCTL4 = 0x08 ; // and set the next interrupt to capture a falling edge.
  }

  else { // This is a falling edge
    falling_edge = TC1;
    TCTL4 = 0x04 ; //Set the next interrupt to capture a rising edge.

		// Return if a lower than maximum freq is set and this sample isn't wanted.
    if ((laserCounter++)%laserFreqDivisor != 0) return;

		// Otherwise calculate distance
		// 2/3 as 1500000 ticks/second -> 3/2 ticks per millimetre measured.
    distance = (2*((falling_edge - rising_edge) / 3)) + LASER_CORRECTION ;

		// If taking multiple samples, combine with existing average.
    if(numLaserSamples > 0) {
        distance = (unsigned int)(((numLaserSamples -1) * (unsigned long)prevAve + distance) / numLaserSamples);
        prevAve = distance;
    }

		// Display new value on 7seg.
    displaySeg(distance/10);
  }
}

/*!
   \brief Setter method for laser frequency divisor.
   \param d New frequency divisor value. LIDAR has maximum frequency of 300Hz,
	 this maximum is divided by d.
   \post Distance may be updated at a different rate.
*/
void set_laser_divisor(unsigned int d) {
    laserFreqDivisor = d;
}

/*!
   \brief Getter method for most-recently computed distance value.
   \pre LIDAR unit must have completed one full measurement cycle, ensure at least
	 100ms have elapsed since timer initialisation and LIDAR unit power-on.
	 \pre Consecutive calls are not guaranteed to return different or updated values;
	 the timing depends on the distance currently detected by the laser due to the PWM
	 encoding of data. If fresh values are required, these should be checked by the calling
	 method, or a delay of at least 40ms between consecutive calls implemented.
   \return Most recent distance value from the LIDAR, expressed in millimeters.
*/
unsigned int getDistance() {
    return distance;
}

/*!
   \brief Sends distance over serial link.
   \pre Serial link must be initialised
	 \pre All as applicable to getDistance().
   \post Send to PC of distance data scheduled for 1 second after call.
*/
void send_range_to_PC(void * ptr) {
    sprintf(get_serial_buf_ptr(), "R(%d)", distance); // Make string
    serial_tx_str_vp((void*)get_serial_buf_ptr());		// Send string
    do_after(1000, send_range_to_PC, (void*)0);				// Reschedule self in 1sec
}
