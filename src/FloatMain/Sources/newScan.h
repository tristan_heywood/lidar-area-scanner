#include "serial.h"
#include "string.h"
#include "lcd.h"

void set_rect_res(int);
void set_rect_laser_samples(int);
void newScan_init(); 

void start_fast_scan(void *);
void right_until_bound(void *);
void rect_left_edge_found(void *);


void set_rect_top_bound(int b);
void set_rect_bottom_bound(int b);
void set_rect_left_bound(int b);
void set_rect_right_bound(int b);