#ifndef CONFIG_H
#define CONFIG_H

#include "Polyscan.h"

#define SCAN_TYPE_RECT 0
#define SCAN_TYPE_POLY 1

#define CONFIG_LOCAL 0
#define CONFIG_SERIAL 1

void set_scan_type(char type);
char get_scan_type();

void set_left_bound(int);
void set_right_bound(int);
void set_top_bound(int);
void set_bottom_bound(int);
void set_laser_freq_divisor(volatile unsigned int);
void set_laser_sample_num(int);
void set_laser_resolution(int);

void init_defaults();

void config();

void introduction();

char config_interface();

void config_scan_type();

void config_left_bound();
void config_right_bound();
void config_top_bound();
void config_bottom_bound();

void config_resolution();
void config_samples();
void config_frequency();

void config_ready();

#endif //CONFIG_H
