/*!
   \file newScan.c
   \brief Implementation of the optimised rectangle scan, rewritten to use the
	 work queue instead of a timer interrupt.
   \author Original implementation by Oscar Fawkes, Tom McFadyen, Emily Zhong.
	 \author Initial rewrite by Tristan Heywood.
	 \author Subsequent edits by all of the above.
   \date 6 June 2017

	 \details The rectangle scan works by firstly scanning coarsely, left to right
	 and top to bottom, until a point on the object is found. Thence, it moves back
	 to accurately measure the left hand edge, then right for the right edge, to
	 determine the object width. Then, it moves up to find the top, and down for the
	 bottom, to establish height. From here, area is calculated as the product of
	 height and width.
*/
#include "servo.h"
#include "timer.h"
#include "newScan.h"
#include "laser.h"
#include <math.h>
#include <stdio.h>

#define NULL ((void*)0) //!< Null pointer
#define FAST_RES 200	//!< Resolution (in hundredths of a degree) to initially find target.
unsigned int SLOW_RES;	//!< Setting for the resolution at which the target will be accurately scanned.

int minRange; //!< Store for the nearest distance to the target yet detected.
int leftBound, rightBound, topBound, bottomBound; //!< Settings for angle limits during scan.
int rLeft, rRight, rTop, rBottom; //!< Store for edges of the target, as detected.
unsigned int nrange; //<! Store for range to calculate area.
volatile double height, width;	//<! Store for height and width of object, for area calculation.
float narea; //<! Store for area.
int rectSamples; //<! Setting for number of samples per orientation.

/*!
   \brief Setter method for samples per orientation.
   \param i Number of samples per orientation.
   \post Setting updated.
*/
void set_rect_laser_samples(int i) {
  rectSamples = i;
}

/*!
   \brief Setter method for scan resolution.
   \param i Resolution for scan process, in hundredths of a degree (centidegrees).
   \post Setting updated.
*/
void set_rect_res(int i) {
  SLOW_RES = i;
}

/*!
   \brief Setter method for top angle bound.
   \param b Top angle bound for scan, in hundredths of a degree (centidegrees).
   \post Setting updated.
*/
void set_rect_top_bound(int b) {
  topBound = b;
}

/*!
   \brief Setter method for bottom angle bound.
   \param b Bottom angle bound for scan, in hundredths of a degree (centidegrees).
   \post Setting updated.
*/
void set_rect_bottom_bound(int b) {
  bottomBound = b;
}

/*!
   \brief Setter method for left angle bound.
   \param b Left angle bound for scan, in hundredths of a degree (centidegrees).
   \post Setting updated.
*/
void set_rect_left_bound(int b) {
  leftBound = b;
}

/*!
   \brief Setter method for right angle bound.
   \param b Right angle bound for scan, in hundredths of a degree (centidegrees).
   \post Setting updated.
*/
void set_rect_right_bound(int b) {
  rightBound = b;
}

/*!
   \brief Sends calculated area to LCD display.
   \pre Scan must have completed and an area calculated before calling.
   \post LCD text updated.
*/
void area_to_LCD() {
  sprintf(get_LCD_buf_ptr(), "Area: %.2fcm2 Dist: %d cm", narea, (minRange /10));
  LCD_put_string(get_LCD_buf_ptr());
}

/*!
   \brief Initialises default scan settings.
   \post Any previously configured settings will be overwritten; do not call this
	 after getting config from user/serial.
*/
void newScan_init() {
  leftBound = -5000;
  rightBound = 5000;
  topBound = 2500;
  bottomBound = -2500;
  SLOW_RES = 50;
  nrange = 0;
}

/*!
   \brief Checks if range is within the bounds required, and updates range stores.
	 \pre Depends on getDistance(), see laser.c.
	 \post Range stores may be updated depending on the current range.
   \return High for valid, low for invalid.
*/
char check_range() {
  unsigned int checkrange;
  checkrange = getDistance(); // Get new value for distance,
  if(checkrange > 800 && checkrange < 1200) {	//If it is within target range,
    if (checkrange < nrange || nrange ==0) {		//And if less than nrange, update
      nrange = checkrange;
    }
    if(checkrange < minRange) {									//And/or if less than minRange, update
        minRange = checkrange;
    }
    return 1;																		//And return true
  }
  return 0;																		// Else return false
}

/*!
   \brief Sends a string over serial interface, with current PTU orientation and LIDAR distance.
   \param format Valid printf format string, with three %d fields to be filled with
	 pan, tilt, and range respectively.
   \pre Serial interface must be initialised.
*/
void print_with_pos(char * format) {
  int pan, tilt, range;
  pan = getPanAngle();
  tilt = getTiltAngle();
  range = getDistance();
  serial_printf(format, pan, tilt, range);
}

/*!
   \brief Tilts PTU down until bottom edge found.
   \param ptr For compatibility reasons, pass a null pointer.
	 \pre Call when PTU pointed at an object and the bottom edge needs finding.
   \post If still pointing at object, will reschedule self and tilt down by SLOW_RES.
	 \post If no longer pointing at object, area will be calculated. Thus, this
	 should be the last step of the rectangle scan process. In this event, width, height,
	 and area will be sent over serial link, and area only printed to the LCD.
*/
void down_until_no_range(void * ptr) {
  if(check_range()) {	// If pointing at object,
    angles_range_over_serial();	// Send to PC
    rBottom = getTiltAngle();		// Update lower angle
    setTiltAngle(getTiltAngle() - SLOW_RES); // Tilt down

		// Depending on how many samples to collect, adjust delay and reschedule.
    do_after(5 * rectSamples + 40, down_until_no_range, NULL);
  }

  else { // Otherwise, must have found bottom edge
    print_with_pos("Bottom edge found: P(%d, %d, %d)");
    stop_disp_servo_LCD();

		// Calculate dimensions, and send to PC
    height = 2*nrange*(tan( (3.14159/180) * (rTop-rBottom)/200 ));
    width = 2*nrange*(tan( (3.14159/180) * (rRight-rLeft)/200 ));

    serial_printf("raw width: %f", width);
    serial_printf("raw height: %f", height);

		// Calculate area, and send to PC.
    narea = (float)(width * height);
    narea /= 100;
    serial_printf("area: %f", narea);
    do_after(1000, area_to_LCD, NULL);
  }
}

/*!
   \brief Tilts PTU up until top edge found.
   \param ptr For compatibility reasons, pass a null pointer.
	 \pre Call when PTU pointed at an object and the top edge needs finding.
   \post If still pointing at object, will update top angle, reschedule self and
	 tilts up by SLOW_RES.
	 \post If no longer pointing at object, PTU tilts down and schedules down_until_no_range().
*/
void up_until_no_range(void * ptr) {
	// See down_until_no_range() for logic explanation
  if(check_range()) {
    angles_range_over_serial();
    rTop = getTiltAngle();
    setTiltAngle(getTiltAngle() + SLOW_RES);
    do_after(5 * rectSamples + 40, up_until_no_range, NULL);
  }
  else {
    print_with_pos("Top edge found: P(%d, %d, %d)");
    setTiltAngle(getTiltAngle() - 400);
    do_after(5 * rectSamples + 500, down_until_no_range, NULL);
  }
}

/*!
   \brief Tilts PTU right until right edge found.
   \param ptr For compatibility reasons, pass a null pointer.
	 \pre Call when PTU pointed at an object and the right edge needs finding.
   \post If still pointing at object, will update right angle, reschedule self and
	 pans right by SLOW_RES.
	 \post If no longer pointing at object, PTU pans left and schedules up_until_no_range().
*/
void right_until_no_range(void * ptr) {
	// See down_until_no_range() for logic explanation
  if(check_range()) {
    angles_range_over_serial();
    rRight = getPanAngle();
    setPanAngle(getPanAngle() + SLOW_RES);
    do_after(5 * rectSamples + 40, right_until_no_range, NULL);
  }
  else {
    print_with_pos("Right edge found: P(%d, %d, %d)");
    setPanAngle(getPanAngle() - 500);
    do_after(5 * rectSamples + 50, up_until_no_range, NULL);
  }
}

/*!
   \brief Tilts PTU left until top edge found.
   \param ptr For compatibility reasons, pass a null pointer.
	 \pre Call when PTU pointed at an object and the left edge needs finding.
   \post If still pointing at object, will update left angle, reschedule self and
	 pan left by SLOW_RES.
	 \post If no longer pointing at object, PTU pans rights and schedules right_until_no_range().
*/
void left_until_no_range(void * ptr) {
	// See down_until_no_range() for logic explanation
  if(check_range()) {
    angles_range_over_serial();
    rLeft = getPanAngle();
    setPanAngle(getPanAngle() - SLOW_RES);
    do_after(5 * rectSamples + 40, left_until_no_range, NULL);
  }
  else {
    print_with_pos("Left edge found: P(%d, %d, %d)");
    setPanAngle(getPanAngle() + 400);
    do_after(5 * rectSamples + 500, right_until_no_range, NULL);
  }
}

/*!
   \brief Begins fine/accurate scan to determine rectangle area.
   \param ptr For compatibility reasons, pass a null pointer.
	 \pre Call when PTU first points at the object during a coarse scan.
   \post Tilts PTU down and begins finding accurate edges.
*/
void rect_left_edge_found(void * ptr) {
	// Tell PC we found it
  print_with_pos("Object found: P(%d, %d, %d)");

	// Move down so we're definitely pointing at the object.
  setTiltAngle(getTiltAngle() - 700);

	// Schedule first movement of slow scan, delay depends on samples to take.
  do_after(5 * rectSamples + 50, left_until_no_range, NULL);
}

/*!
   \brief Coarse scan implementation.
   \param ptr For compatibility reasons, pass a null pointer.
   \pre PTU should be set pointing to the upper left bound.
   \post Will continue to reschedule self until PTU pointing at the object.
*/
void right_until_bound(void * ptr) {
		// If distance in range, target found.
    if(check_range()) {
        rect_left_edge_found(NULL);
        return;
    }

		// If we've reached the right scan edge, go back to the left and tilt down,
		// then repeat.
    if(getPanAngle() + FAST_RES > rightBound) {
        setTiltAngle(getTiltAngle() - 500);
        setPanAngle(leftBound);
        do_after(5 * rectSamples + 500, right_until_bound, NULL);
    }
		// Otherwise move right and check again.
    else {
        setPanAngle(getPanAngle() + FAST_RES);
        do_after(5 * rectSamples + 40, right_until_bound, NULL);
    }
}

/*!
   \brief Call to commence the rectangle scan.
   \param ptr For compatibility reasons, pass a null pointer.
   \pre Scan settings should be configured.
   \post First actions for scan routine scheduled on work queue.
*/
void start_fast_scan(void * ptr) {
    serial_printf("lb: %d, rb: %d", leftBound, rightBound);
    narea = 0;
    setPanAngle(leftBound);
    setTiltAngle(2000);
    start_disp_servo_LCD();
    minRange = 10000;
    do_after(5 * rectSamples + 1000, right_until_bound, NULL);
}
