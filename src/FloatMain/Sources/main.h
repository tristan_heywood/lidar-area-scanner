#include <hidef.h>            /* common defines and macros */
#include "derivative.h"       /* derivative-specific definitions */

#include "config.h"
#include "lcd.h"
#include <stdio.h>
#include "servo.h"
#include "iic_controller.h"
#include "iic.h"
#include "serial.h"
#include "string.h"
#include "timer.h"
#include <math.h>
#include "segdisplay.h"
#include <stdlib.h>
#include "newScan.h"
#include "gyro.h"



void call_do_later_macro(void *(func)(void*), void*, unsigned long int);
void newScan_init();
