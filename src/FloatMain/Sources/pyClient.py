import serial
import queue
import threading
import time
from random import randrange
import cloud
import sys
import pdb
from math import *

#Starts the GUI. This method will never return so must be called from a thread
def start_display(disp) :
	disp.display()

#The first commmand line argument is a number representing the com port over which the device is connected
comPort = 'COM4'
if len(sys.argv) >= 2 :
	comPort = 'COM' + sys.argv[1]

plotQ = queue.Queue() #queue of tuples of the form (x, y, z), representing coordinates which will be plotted in the real time point cloud and logged
inputQ = queue.Queue() #Queue of messages to be sent over serial to the device, formatted in accordance with the documentation on the serial module
consoleQ = queue.Queue() #Queue of messages recieved from device over serial, will be displayed in the GUI

disp = cloud.Display(plotQ, inputQ, consoleQ) #A Display object is used to handle the GUI

thr = threading.Thread(target = start_display, kwargs = {"disp" : disp}) #start GUI in a thread
thr.daemon = True
thr.start()

ser = serial.Serial(
	port = comPort,
	baudrate=19200,
	parity = serial.PARITY_ODD,
	stopbits = serial.STOPBITS_TWO,
	bytesize = serial.SEVENBITS)
print(ser.name)

#Recieves input from the console and adds it to the inputQ to be sent to the device, must be called from a thread
def userInput() :
	while True :
		inStr = input()
		if inStr[0] == 'q' :
			sys.exit(0)
		inputQ.put(inStr)

inthr = threading.Thread(target = userInput) #create a thread to read input from the console
inthr.daemon = True
inthr.start()


recv = b''
recvStr = ''

#Main thread will now read input from the device over serial and add it to plotQ and consoleQ as required, and send over serial
while True :
	#if there are commands to send, send them
	if not inputQ.empty() > 0 :
		toSend = inputQ.get().encode('ascii')
		print("SENDING: [" + str(toSend) + ']')
		consoleQ.put("SENDING: [" + str(toSend) + ']')
		ser.write(toSend)

	#if there is data from the device to read, read it
	if ser.inWaiting() > 0 :
		recv = ser.read(ser.inWaiting())
		recvStr += str(recv)[2:-1]
		if '_' in recvStr : #if there is a complete message in the input (since all messages end with _)
			token, recvStr = recvStr.split('_', 1) #take this message off the input string
			print("RECIEVED: [" + token + "]")
			consoleQ.put('RECIEVED: [' + token + ']')
			if token.startswith('P('): #check if this token is a coordinate points - in which case it should be added to plotQ
				try :
					nums = token[token.find('(')+1 : token.find(')')]
					pan, tilt, dist = [float(x.strip()) for x in nums.split(',')]
				except:
					pass
				#convert (pan, tilt, range) into (x, y, z) coordinates
				xCo = int((dist / 1000 * sin(radians(tilt  / 100))) * 100)
				zCo = int((dist / 1000) * cos(radians(tilt  / 100)) * 100)
				yCo = int((dist / 1000) * sin(radians(pan / 100)) * 100)
				plotQ.put((xCo, yCo, zCo))

	time.sleep(0.001) #sleep briefly so we don't waste CPU cycles spinning on an empty buffer
