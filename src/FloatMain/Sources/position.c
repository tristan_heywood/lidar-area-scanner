/*!
   \file position.c
   \brief Calculation of PTU orientation from IMU.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong.
   \date 6 June 2017
*/

#include "derivative"
#include "servo.h"
#include "laser.h"
#include <math.h>
#include "timer.h"
#include "iic_controller.h"
#include "gyro.h"
#include "serial.h"
#include <stdio.h>
#include "lcd.h"

#define NULL ((void*)0)			//!< Null pointer
#define GYRO_CONSTANT 10150	//!< Constant of integration for gyroscope

volatile int 	azimuth,		//!< Current azimuth estimate based on gyro integration.
							elevation;	//!< Current elevation estimate based on gyro integration.

/*!
   \brief Initialises attitude and heading reference system.
   \post State variables readied for updates, first update scheduled.
*/
void init_ahrs() {
	azimuth = 0;
	elevation = 0;
	do_after(0, update_ahrs, NULL);
	do_after(1000, gyro_pos_to_LCD, NULL);
}

/*!
   \brief Updates azimuth and elevation by integrating raw gyroscope output.
   \pre Must have called init_ahrs(), IIC must be initialised.
   \post Azimuth and elevation may be updated, future update scheduled.
*/
void update_ahrs() {
		volatile int gxRaw, gyRaw, gzRaw;	//!< Gyro

    //Reschedule self
    do_after(10, update_ahrs, NULL);

    //Get fresh data
    l3g4200d_getrawdata(&gxRaw, &gyRaw, &gzRaw);

    if(abs(gyRaw) > 50) { //if we're not drifting
        elevation += (int)(((float)gyRaw)/(float)GYRO_CONSTANT);
    }
    if(abs(gxRaw) > 50) {
        azimuth -= (int)(((float)gxRaw)/(float)10150);
    }
}

/*!
   \brief Prints current gyroscope orientation estimation to LCD.
   \param ptr For compatibility pass null pointer.
   \post LCD text changed, future update scheduled for 1000ms hence.
*/
void gyro_pos_to_LCD(void * ptr) {
		sprintf(get_LCD_buf_ptr(), "Az: %d deg       El: %d deg", azimuth, elevation);
		LCD_put_string(get_LCD_buf_ptr());
		do_after(1000, gyro_pos_to_LCD, NULL);
}

/*!
   \brief Prints current heading based on magnetometer values to LCD.
   \param ptr For compatibility pass null pointer.
   \pre IIC must be initialised.
   \post LCD text changed, future update scheduled for 1000ms hence.
*/
void heading_to_LCD(void * ptr) {
    int mxraw, myraw, mzraw;
    float heading;
    hm5883_getrawdata(&mxraw, &myraw, &mzraw);
    heading = atan(((float)mzraw)/((float)myraw)) * 57.30;
    sprintf(get_LCD_buf_ptr(), "Heading: %.2f deg", heading);
    LCD_put_string(get_LCD_buf_ptr());
    do_after(1000, heading_to_LCD, NULL);
}

/*!
   \brief Prints current PTU elevation angle based on accelerometer output to LCD.
   \param ptr For compatibility pass null pointer.
   \pre IIC must be initialised.
   \post LCD text changed, future update scheduled for 1000ms hence.
*/
void elevation_to_LCD(void * ptr) {
    int axraw;
    int ayraw;
    int azraw;
    float elev;

    adxl345_getrawdata(&axraw, &ayraw, &azraw);
    elev = atan(((float)azraw)/((float)axraw)) * 57.30;
    sprintf(get_LCD_buf_ptr(), "E(%f)", elev);
    LCD_put_string(get_LCD_buf_ptr());
    do_after(1000, elevation_to_LCD, NULL);
}
