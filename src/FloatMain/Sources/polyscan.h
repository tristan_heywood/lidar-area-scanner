#include "timer.h"
#include "serial.h"
#include "math.h"
#include "lcd.h"

void set_poly_left_bound(int);
void set_poly_right_bound(int);
void set_poly_top_bound(int);
void set_poly_bottom_bound(int);

void set_num_samples(int); 
void set_poly_res(int);

void start_poly_scan();
void polyScan_init();
void pLeft_until_bound();
void pRight_until_bound();
char poly_check_range();
void start_poly_fine_scan();
