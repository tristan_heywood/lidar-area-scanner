/*!
   \file iic_controller.c
   \brief IIC interface methods to obtain raw data from IMU, as provided.
   \author Eduardo Nebot
   \date 29 January 2016
*/

// Demonstration functions for IIC to read inertial sensor values
//
// The program will send read values to the serial port.
// you need to connect the serial port to a terminal to verify operation
// port speed 9600 bauds
//
// Eduardo Nebot  29 January 2016
// Sensors implemented:
// Gryro L2g2400D  (3 axis )
// Accelerometer  ADXL345
// Magnetometer HM5883
// Laser Lidarlight:  the driver is working with version 1 but not with version 2
// Version 2 work in progress:
// Laser Interface will be done by measuring the pulse PWM pulse width
// Last version: 29/1/16
//
//  This version installed interrupts with the simple model
//  The user should create a project selecting small memory model
//  and minimal startup code. This form will not intiliase any variables !
//  Your program will need to intialize the variables !
//
// Resources used: This program is using Timer 6 for the sampling time
//
// the iic drivers are using Timer 7. ( You cannot use this timer in your program)
// Do not change the prescaler. If you do you need to change some code in iic.c
//
//


#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "iic.h"
#include "l3g4200.h"
#include "iic_controller.h"



volatile unsigned char alarmSignaled1 = 0; /* Flag set when alarm 1 signaled */

volatile unsigned int currentTime1 = 0; /* variables private to timeout routines */
unsigned int alarmTime1 = 0;
volatile unsigned char alarmSet1 = 0;

unsigned char buff[BUFF_SIZE];
int gxraw[BUFF_SIZE];
int gyraw[BUFF_SIZE],gzraw[BUFF_SIZE];

int axraw[BUFF_SIZE];
int ayraw[BUFF_SIZE],azraw[BUFF_SIZE];

int mxraw[BUFF_SIZE];
int myraw[BUFF_SIZE],mzraw[BUFF_SIZE];

int k;

unsigned int Dist;

void intertial_init(void) {
    //PLL_Init();  // make sure we are runnign at 24 Mhz todo : removing this might be a mistake
    Init_TC6();

    iicinit();
    gyro_init();     // l3g4200 setup
    accel_init();
    magnet_init();
}
//
// void main(void) {
//   /* put your own code here */
//
//  unsigned char  myString[20];
//  unsigned char unsigned character;
//
//  unsigned char D2W, D3R, WHO, who;
//  int  res1, res2,  res3, *p;
//  float acc;
//
//
//
//  // The next 4 lines just to make sure all is working
//  // are not needed for final program
//
//  DDRB= 0xFF;   /* Port B output */
//  DDRJ= 0xFF;   // Port J to Output
//  PTJ = 0x00;   // enable LEDs
//  PORTB=0x55;     // debuging info
//
//
//
//  PLL_Init();  // make sure we are runnign at 24 Mhz
//
//
// EnableInterrupts;
//
// //This program will send the gyro, accelerometer adn magnetometer data
// // to a serial port
// // You can connect the serial port, set it to 9600 bauds
//
//  SCI1_Init(BAUD_9600);   // capped at 9600, if PLL inactive (4 MHz bus)
//
//  SCI1_OutString("Program Starting ");      // should display this
//
//
//  Init_TC6();
//
//  iicinit();
//
//
//
//
// // gyro_test(); // make sure a l3g is connected
//
//
//
//
//  while(1) {
//
//  delay1(500);
//
//
//
// // L3G4200d Gyro;
//
//  // l3g4200d_getrawdata( &gxraw, &gyraw, &gzraw) ;        // read data
//  //
//  // SCI1_OutString("Gyro Gx:");
//  // SCI1_OutUDec((unsigned short) gxraw[0]);
//  // SCI1_OutString(" Gy:");
//  // SCI1_OutUDec((unsigned short) gyraw[0]) ;
//  // SCI1_OutString(" Gz:");
//  // SCI1_OutUDec((unsigned short) gzraw[0]) ;
//  //
//  // SCI1_OutString("\r\n");
//
//
//  // ADCL345 Accelerometer
//
//   adxl345_getrawdata( &axraw, &ayraw, &azraw) ;        // read data
//   SCI1_OutString("Accel Ax:");
//   SCI1_OutUDec((int) axraw[0]);
//   SCI1_OutString(" Ay:");
//   SCI1_OutUDec((int) ayraw[0]) ;
//   SCI1_OutString(" Az:");
//   SCI1_OutUDec((int) azraw[0]) ;
//
//   SCI1_OutString("\r\n");
//
//
//
//  // HM5883_magnetometer
//
//  // hm5883_getrawdata(&mxraw, &myraw, &mzraw);
//  //
//  // SCI1_OutString("Magn Mx:");
//  // SCI1_OutUDec((unsigned short) mxraw[0]);
//  // SCI1_OutString(" My:");
//  // SCI1_OutUDec((unsigned short) myraw[0]) ;
//  // SCI1_OutString(" Mz:");
//  // SCI1_OutUDec((unsigned short) mzraw[0]) ;
//  //
//  // SCI1_OutString("\r\n");
//
//  }
//
//
//    /*    Not using the laser IIC, not compatible with this lasr
//
//
//   laser_data (&Dist);
//
//
//   SCI1_OutString("\r\n laser:");
//
//   SCI1_OutUDec((unsigned short) Dist) ;
//
//   SCI1_OutString("\n\r");
//
//
//   */
//
// }




//   ******************  END Main   *****************





// Magnetometer

void magnet_init(void){

  int  res1;
  res1=iicstart(magnet_wr);
  res1=iictransmit(HM5883_MODE_REG );  //
  res1=iictransmit(0x00 );
  iicstop();

}


void magnet_test(void){

}

void hm5883_getrawdata(int *mxraw, int *myraw, int *mzraw){

 unsigned char i = 0;
 unsigned char buff[6];
 int res1;

 res1=iicstart(magnet_wr);
 res1=iictransmit(HM5883_DATAX0 );
 res1= iicrestart(magnet_rd);
 iicswrcv();

 for(i=0; i<4  ;i++) {
  buff[i]=iicreceive();
 }

 buff[i]= iicreceivem1();
 buff[i+1]= iicreceivelast();

	*mxraw = ((buff[0] << 8) | buff[1]);
	*myraw = ((buff[2] << 8) | buff[3]);
	*mzraw = ((buff[4] << 8) | buff[5]);
}




void accel_test(void){}


void accel_init (void){

 int  res1;

 res1=iicstart(accel_wr);
 res1=iictransmit(ADXL345_POWER_CTL );  //
 res1=iictransmit(0x08 );

 res1=iictransmit(ADXL345_DATA_FORMAT );  // ;
 res1=iictransmit(0x08 );

 iicstop();
}


void adxl345_getrawdata(int *axraw, int *ayraw, int *azraw){

 unsigned char i = 0;
 unsigned char buff[6];
 int res1;

 res1=iicstart(accel_wr);
 res1=iictransmit(ADXL345_DATAX0 );
 res1= iicrestart(accel_rd);
 iicswrcv();

 for(i=0; i<4  ;i++) {
  buff[i]=iicreceive();
 }

 buff[i]= iicreceivem1();
 buff[i+1]= iicreceivelast();

	*axraw = ((buff[1] << 8) | buff[0]);
	*ayraw = ((buff[3] << 8) | buff[2]);
	*azraw = ((buff[5] << 8) | buff[4]);
}



// test the precense of Gyro , should get 211 on return
//

void gyro_test(void) {
 int res1,who;

 res1=iicstart(0xD2);
 res1=iictransmit(L3G4200D_WHO_AM_I);

 res1=iicrestart(0xD3);
 who=iicreceiveone();
 //who=who & 0x00ff;     Debugging  info
 //PORTB=  who ;

}


 //  Gyro Initialisation

 void gyro_init (void) {

 int  res1;

 res1=iicstart(gyro_wr);
 res1=iictransmit(L3G4200D_CTRL_REG1 );  // ; 100hz, 12.5Hz, Power up
 res1=iictransmit(0x0f );
 iicstop();
 }


// Function to get a set of gyro data
// Eduardo Nebot,30 July 2015

void l3g4200d_getrawdata(int *gxraw, int *gyraw, int *gzraw) {
 	unsigned char i = 0;
	unsigned char buff[6];
	int res1;

   res1=iicstart(gyro_wr);
   res1=iictransmit(L3G4200D_OUT_XYZ_CONT );
   res1= iicrestart(gyro_rd);

 iicswrcv();

 for(i=0; i<4  ;i++) {
  buff[i]=iicreceive();
 }

buff[i]= iicreceivem1();
buff[i+1]= iicreceivelast();

	*gxraw = ((buff[1] << 8) | buff[0]);
	*gyraw = ((buff[3] << 8) | buff[2]);
	*gzraw = ((buff[5] << 8) | buff[4]);
}

// ********************





 // Laser  initialisation, Eduardo nebot , 2/8/15



 void laser_init (void) {

 int  res1;



 //k=iicstart(LidarWriteAddr);


k=iicstart(laser_wr);   // 0xc4
//delay1(10);

k=iictransmit(0x00 );  // ; write register 0x00 with value 0x04
                          // (This performs a DC stabilization cycle,
                          // Signal Acquisition, Data processing).
                          //Refer to the section �I2C Protocol Summary
                          // in this manual for more information about
                          // I2C Communications


//delay1(10);
 k=iictransmit(0x04 );
 iicstop();
 }



 // Laser IIC Function  ( Does not work with current laser )
 // Read the PWM signal

void laser_data(unsigned int *Dist) {

	unsigned char buff[2],k1;
	int res1;

  laser_init();

  delay1(20);      //20 before

  k=iicstart(laser_wr);    //0xc4

  k=iictransmit(0x8f);

  delay1(20);

  k= iicrestart(laser_rd);   //c5
 //   delay1(10)  ;

  iicswrcv();
  delay1(20);
   buff[0]= iicreceivem1();      // read 2 bytes
   buff[1]= iicreceivelast();

	*Dist = ((buff[0] << 8) | buff[1]);

//   delay1(3)  ;



 PORTB=*Dist &0x00ff;     //debugging


}

// ********************












void setAlarm1(unsigned int msDelay1)
{
    alarmTime1 = currentTime1 + msDelay1;
    alarmSet1 = 1;
    alarmSignaled1 = 0;
}


void delay1(unsigned int msec)
{
    TC6 = TCNT + 24000; // Set initial time
    setAlarm1(msec);
    while(!alarmSignaled1) {};
}



/*  Interrupt   EMN */

// interrupt(((0x10000-Vtimch7)/2)-1) void TC7_ISR(void){
// the line above is to make it portable between differen
// Freescale processors
// The symbols for each interrupt ( in this case Vtimch7 )'
// are defined in the provided variable definition file
// I am usign a much simpler definition ( vector number)
// that is easier to understand

interrupt 14 void TC6_ISR(void) {

  TC6 =TCNT + 24000;   // interrupt every msec
  TFLG1=TFLG1 | TFLG1_C6F_MASK;
  currentTime1++;
    if (alarmSet1 && currentTime1 == alarmTime1)
    {
        alarmSignaled1 = 1;
        alarmSet1 = 0;
    }
   //PORTB=PORTB+1;        // count   (debugging)
}



void Init_TC6 (void) {

_asm SEI;

TSCR1=0x80;
TSCR2=0x00;   // prescaler 1
TIOS=TIOS | TIOS_IOS6_MASK;
TCTL1=0x40;
TIE=TIE | 0x40;;

 _asm CLI;

}
