/*!
   \file polyscan.c
   \brief Implementation of the general scan to scan a polygon (with holes). Uses
	 work queue scheduling.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong.
   \date 6 June 2017

	 \details The polygon scan first scans everything within its bounds, to establish the approximate
	 dimensions of the target object. Thence, it scans slowly within these approximate
	 bounds, left to right, in 'rows', measuring the area of horizontal rectangles, or 'strips'.
	 These are summed from top to bottom to obtain the area of the shape.
*/

#include "polyScan.h"
#include <stdio.h>

#define TRUE 1
#define FALSE 0

#define MAX(A, B) ((A) > (B) ? (A) : (B))
#define MIN(A, B) ((A) < (B) ? (A) : (B))

#define MINIMUM_INTERVAL 200 		//!< The minimum width of the object at its bottom edge, in centidegrees.
#define HEIGHT_CORRECTION 1.05 	//!< Scale factor for height, should be tuned experimentally.

int polyLeftBound,		//!< Left search bound.
		polyRightBound,		//!< Right search bound.
		polyTopBound,			//!< Top search bound.
		polyBottomBound;	//!< Bottom search bound.

int POLY_FAST_RES;				//!< Initial fast scan resolution.
int POLY_FINE_RES;				//!< Accurate slow scan resolution, for pan axis.
int POLY_FINE_VERT_SCALE;	//!< Multiple of POLY_FINE_RES to use for tilt axis.

int laserSamples;	//!< Setting for laser samples per orientation
int pMinDist;			//!< Minimum distance to object yet found.
int pLeft,				//!< Pan angle of left edge approximation.
		pRight,				//!< Pan angle of right edge approximation.
		pTop,					//!< Tilt angle of top edge approximation.
		pBottom;			//!< Tilt angle of bottom edge approximation.

float areaTot;											//!< Total area so far measured.
unsigned long int intervalThisRow;	//!< Total width of current row, in centidegrees.
unsigned long int totRangeThisRow;	//!< Sum of all range samples in this row, to find average.
unsigned int numSamplesThisRow;			//!< Number of in-range samples taken in current row.
unsigned int intervalStart;					//!< Start pan angle of the current interval.
unsigned int prevInterval;					//!< Width of the previous row, in centidegrees.
char inInterval;										//!< High if currently measuring an interval,
																		//!< low if not pointing at object or in hole.

/*!
   \brief Setter method for number of samples per orientation.
   \param i Number of samples per orientation.
   \post Setting changed.
*/
void set_num_samples(int i) {
    laserSamples = i;
}

/*!
   \brief Setter method for top bound angle.
   \param b Upper angle bound in centidegrees.
   \post Setting changed.
*/
void set_poly_top_bound(int b) {
    polyTopBound = b;
}

/*!
   \brief Setter method for bottom bound angle.
   \param b Lower angle bound in centidegrees.
   \post Setting changed.
*/
void set_poly_bottom_bound(int b) {
    polyBottomBound = b;
}

/*!
   \brief Setter method for left bound angle.
   \param b Left angle bound in centidegrees.
   \post Setting changed.
*/
void set_poly_left_bound(int b) {
    polyLeftBound = b;
}

/*!
   \brief Setter method for right bound angle.
   \param b Right angle bound in centidegrees.
   \post Setting changed.
*/
void set_poly_right_bound(int b) {
    polyRightBound = b;
}

/*!
   \brief Setter method for fast scan resolution.
   \param i Fast scan resolution in centidegrees per step.
   \post Setting changed.
*/
void set_poly_res(int i) {
    POLY_FAST_RES = i;
}

/*!
   \brief Initialises polygon scan with default settings.
   \post All settings reset to defaults, so do not call after user has entered
	 configuration.
*/
void polyScan_init() {
    polyLeftBound = -5000;
    polyRightBound = 5000;
    polyTopBound = 2500;
    polyBottomBound = -2500;
    laserSamples = 0;

    POLY_FAST_RES = 600;
    POLY_FINE_RES = 50;
    POLY_FINE_VERT_SCALE = 3;
    pLeft = 10000;
    pRight = -10000;
    pTop = -10000;
    pBottom = 10000;
}

/*!
   \brief Sets edge approximations.
   \param pan New pan angle to update left and right edge approximations.
	 \param tilt New tilt angle to update top and bottom edge approximations.
   \post If pan is greater than the right approximation, or lesser than the left
	 approximation, the corresponding approximation will be set to pan.
	 \post The same applies to tilt for the upper and lower approximations.
*/
void update_extremes(int pan, int tilt) {
    pLeft = MIN(pan, pLeft);
    pRight = MAX(pan, pRight);
    pTop = MAX(tilt, pTop);
    pBottom = MIN(tilt, pBottom);
}

/*!
   \brief Sends calculated area to the LCD display.
   \pre Scan must be complete and area calculated.
   \post LCD text changed.
*/
void pArea_to_LCD() {
    sprintf(get_LCD_buf_ptr(), "Area: %.2fcm2", areaTot / 100);
    LCD_put_string(get_LCD_buf_ptr());
}

/*!
   \brief Pans PTU right until reaching the right bound, updating extremes as found.
   \param ptr For compatibility reasons, pass a null pointer.
   \pre PTU should be pointing toward the top left bound before the first call.
   \post Another movement or the second stage of scanning will be scheduled.
*/
void pRight_until_bound(void * ptr) {
    angles_range_over_serial(); // Send to PC for point cloud

		// If in range, apply this point to the extremes (if necessary)
    if(poly_check_range()) {
        update_extremes(getPanAngle(), getTiltAngle());
    }

		// If at right bound:
    if(getPanAngle() + POLY_FAST_RES > polyRightBound) {
				// Start fine scan if also at bottom bound
        if(getTiltAngle() - POLY_FAST_RES < polyBottomBound) {
            serial_tx_str_vp((void*)"Poly fast scan complete");
            start_poly_fine_scan();
            return;
        }
				//Otherwise, tilt down and continue scanning quickly
        setTiltAngle(getTiltAngle() - POLY_FAST_RES);
        setPanAngle(polyRightBound);
        do_after(5 * laserSamples + 100, pLeft_until_bound, NULL);
    }

		// Otherwise continue moving
    else {
        setPanAngle(getPanAngle() + POLY_FAST_RES);
        do_after(5 * laserSamples + 30, pRight_until_bound, NULL);
    }
}

/*!
   \brief Pans PTU left left until reaching the left bound, updating extremes as found.
	 Identical to pRight_until_bound(), but opposite in direction.
   \param ptr For compatibility reasons, pass a null pointer.
   \post Another movement or second stage of scanning will be scheduled.
*/
void pLeft_until_bound(void * ptr) {
		// Same logic as pRight_until_bound()
    angles_range_over_serial();
    if(poly_check_range()) {
        update_extremes(getPanAngle(), getTiltAngle());
    }
    if(getPanAngle() - POLY_FAST_RES < polyLeftBound) {
        if(getTiltAngle() - POLY_FAST_RES < polyBottomBound) {
            serial_tx_str_vp((void*)"Poly fast scan complete");
            start_poly_fine_scan();
            return;
        }
        setTiltAngle(getTiltAngle() - POLY_FAST_RES);
        setPanAngle(polyLeftBound);
        do_after(5 * laserSamples + 100, pRight_until_bound, NULL);
    }
    else {
        setPanAngle(getPanAngle() - POLY_FAST_RES);
        do_after(5 * laserSamples + 30, pLeft_until_bound, NULL);
    }
}

/*!
   \brief Prepares for a new polygon scan.
   \pre All settings should be configured.
   \post Commencement of scan scheduled on work queue.
*/
void start_poly_scan() {
    areaTot = 0;
    inInterval = 0;
    intervalThisRow = 0;
    totRangeThisRow = 0;
    numSamplesThisRow = 0;
    prevInterval = 0;

    setPanAngle(polyLeftBound);
    setTiltAngle(polyTopBound);
    start_disp_servo_LCD();
    do_after(5 * laserSamples + 400, pRight_until_bound, NULL);
}

/*!
   \brief Continuously scans rows left to right, measuring area.
   \param ptr For compatibility reasons, pass a null pointer.
   \pre Approximate bounds detected and set, PTU pointing to the top-left bound.
   \post Reschedules self until area scan complete.
*/
void pfRight_until_bound(void * ptr) {
    volatile unsigned int aveRange;
    volatile float width;
    volatile float height;
    volatile float stripArea;
    angles_range_over_serial();

		// If in range, start an interval if not already, and add new data.
    if(poly_check_range()) {
        if(!inInterval) { //if we are not already in an interval, this is the start of an interval
            intervalStart = getPanAngle();
            inInterval = TRUE;
        }
        totRangeThisRow += getDistance();
        numSamplesThisRow++;
    }
    else {
        if(inInterval) { //if were were in an interval and are now out of range, we are no longer in an interval
            intervalThisRow += (getPanAngle() - intervalStart);
            inInterval = FALSE;
        }
    }

		// If at right bound...
    if(getPanAngle() + POLY_FINE_RES > pRight) {
        if(inInterval) {
						// Should never get here, intervals should be within bounds
            // todo: report error, notify user bounds should be expanded.
        }

				//If at bottom bound, scan complete. Calculate area and return.
        if(getTiltAngle() - POLY_FINE_RES < pBottom) {
            serial_printf("total area: %f", areaTot);
            serial_tx_str_vp((void*)"Poly fine scan complete");
            stop_disp_servo_LCD();
            do_after(1000, pArea_to_LCD, NULL);
            return;
        }

        // Scan might end earlier, check if we've already found the entire object
				// based on the ratio of this row to last (pole much narrower than bottom).
        if(prevInterval / (intervalThisRow + 1) >= 4 || (areaTot > (float)40000 && intervalThisRow < 400)) {
            serial_printf("total area: %f   dist: %d cm", areaTot, pMinDist / 100);
            serial_tx_str_vp((void*)"Poly fine scan complete");
            stop_disp_servo_LCD();
            do_after(1000, pArea_to_LCD, NULL);
            return;
        }

				// Otherwise, add this row to the cumulative scan results
        if(intervalThisRow > MINIMUM_INTERVAL) {
            aveRange = totRangeThisRow / numSamplesThisRow;
            width = (float)intervalThisRow * (float)aveRange / 5729.6;
            height = ((float)(POLY_FINE_VERT_SCALE * POLY_FINE_RES) * (float)aveRange) / 5729.6 * HEIGHT_CORRECTION;
            stripArea = width * height;
            areaTot += stripArea;
            serial_printf("row width: %f", width);
            serial_printf("strip area: %f", stripArea);
            prevInterval = intervalThisRow;
            totRangeThisRow = 0;
            numSamplesThisRow = 0;
            intervalThisRow = 0;

        }

				// Continue scanning
        setPanAngle(pLeft);
        setTiltAngle(getTiltAngle() - POLY_FINE_VERT_SCALE * POLY_FINE_RES);
        do_after(5 * laserSamples + 200, pfRight_until_bound, NULL);
    }

		//Otherwise Continue scanning
    else {
        setPanAngle(getPanAngle() + POLY_FINE_RES);
        do_after(5 * laserSamples + 40, pfRight_until_bound, NULL);
    }
}

/*!
   \brief Prepares for start of fine area scan after initial coarse scan complete.
   \pre Completion of fast scan, with object bound approximations set.
   \post Fine area scan scheduled on work queue.
*/
void start_poly_fine_scan() {
    pLeft -= POLY_FAST_RES;
    pRight += POLY_FAST_RES;
    pTop += POLY_FAST_RES;
    pBottom -= POLY_FAST_RES;
    pMinDist = 10000;
    serial_printf("left: %d, right: %d, top: %d, bottom %d", pLeft, pRight, pTop, pBottom);

    // pLeft = -2000;
    // pRight = 2000;
    // pTop = 2000;
    // pBottom = -2000;

    setPanAngle(pLeft);
    setTiltAngle(pTop);
    do_after(5 * laserSamples + 400, pfRight_until_bound, NULL);
}

/*!
   \brief Checks if range is within the bounds required, and updates range store.
	 \pre Depends on getDistance(), see laser.c.
	 \post Range store may be updated depending on the current range.
   \return High for valid, low for invalid.
*/
char poly_check_range() {
    unsigned int checkrange;
    checkrange = getDistance();
    if(checkrange > 800 && checkrange < 1200) {
        if(checkrange < pMinDist) {
         pMinDist = checkrange;
        }
        return (char)1;
    }
    return (char)0;
}
