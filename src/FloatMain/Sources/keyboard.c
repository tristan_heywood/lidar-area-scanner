/*!
   \file keyboard.c
   \brief Implementation of Dragon12 keypad to get button presses from three
	 different keys. Includes debouncing implementation.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong
   \date 6 June 2017
*/

#include "derivative.h"
#include "keyboard.h"
#include "lcd.h"

volatile unsigned long int debouncer;
//!< Loop counter for current debouncing sampling period.

volatile unsigned long int incrCount, decrCount, enterCount;
//!< Counters for number of detections in current sample period.

/*!
   \brief Prepares Port A for reading input on the rightmost column.
   \post Low nibble of Port A configured for output, and bit 3 set high. High nibble
	 configured for input.
*/
void init_keyboard(void)
{
  DDRA = 0x0F ; //Set the top 4 ports to input, and the bottom 4 to output.
  PORTA = 0x08; // Set PA3 high so we only read right column of keyboard
}

/*!
   \brief Gets the next valid keypress on the keyboard
   \pre Keyboard must be initialised with init_keyboard().
   \return One of INCREMENT, DECREMENT, or ENTER.
*/
Key get_next_key() {
	MSDelay(10000);
	while (1) {
		// Take lots of samples of port A
  	for(debouncer = 100000, incrCount = 0, decrCount = 0, enterCount = 0; debouncer != 0; debouncer--) {
  	  switch (PORTA & 0xBF) { // Bitwise & to ignore 4th button in column
  	    case 0x18: incrCount++; break;
  	    case 0x28: decrCount++; break;
  	    case 0x88: enterCount++; break;
  	  }
  	}

		// If the count for any is significant, return it
		// NOTE: must be tuned per board, different values
		// 				may be required.
  	if (incrCount > 50000) {
  	  return incrEMENT;
  	} else if (decrCount > 70000) {
	    return DECREMENT;
  	} else if (enterCount > 80000) {
  	  return ENTER;
  	}
	}
}
