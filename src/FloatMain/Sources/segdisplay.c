/*!
   \file segdisplay.c
   \brief Controls the seven segment displays.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong.
   \date 6 June 2017
*/

#include "segdisplay.h"
#include "derivative.h"

//! Constants for Port P to enable one 7seg display at a time.
const char screens[4] = {0xFE, 0xFD, 0xFB, 0xF7};

//! Constants for each decimal digit to display in 7seg format.
const char lookup[10] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F,
                         0x6F};

unsigned char current_seg;	//!< Which 7seg is currently illuminated.
unsigned int value;					//!< The value to display on the 7seg display.
unsigned int tmp;						//!< Copy of value to manipulate for decimal conversion.

/*!
   \brief Initialises seven segment display.
   \post Seven segment displays enabled, value zeroed so nothing displayed.
*/
void initSegDisplay() {
  DDRB = 0xFF;
  DDRP = 0xFF;
	value = 0;
}

/*!
   \brief Setter method for value to be displayed.
   \param newvalue New value to be displayed, should be <= 9999, else display
	 will show (newvalue % 10000).
   \pre initSegDisplay() must have been called.
   \post Setting and displayed value changed.
*/
void displaySeg(unsigned int newvalue) {
  value = newvalue;
}

/*!
   \brief Interrupt subroutine to update 7seg display, to multiplex output.
	 Should be called at >200Hz to ensure clear readout.
   \post The segment illuminated will be changed, with the appropriate digit.
*/
interrupt 11 void TC3_LED_ISR(void) {
  TFLG1 |= TFLG1_C3F_MASK; //clear interrupt flag for timer 3
  TC3 += 5000;	// Call again in 3.33ms
  PORTB = 0;		// Turn off current segment

	// If starting a new cycle of display, reset tmp to value.
  if (current_seg == 0) {
    tmp = value;
  }

	// Turn on next segment, with correct value.
  PTP = screens[3-current_seg];
  PORTB = (tmp>0) ? lookup[tmp % 10] : 0; // Segment off if a leading zero.

	// Int division by 10 for to get next decimal digit in least significant position.
  tmp /= 10;

	// Increment to next segment, or reset if finished cycle.
  if (++current_seg > 3) {
    current_seg = 0;
  }
}
