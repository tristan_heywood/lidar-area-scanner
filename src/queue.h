#ifndef QUEUE_H
#define QUEUE_H

#include <hidef.h>            /* common defines and macros */
#include "derivative.h"       /* derivative-specific definitions */
#include "lcd.c"

void (*workQueue[WORK_QUEUE_LENGTH])(void *);
unsigned char prodInd; //producer index, the next position to write work to
unsigned char workerInd; //worker index, the next position to pull work from
void * workData[WORK_QUEUE_LENGTH];

#define BUFFER_LENGTH 100
#define WORK_QUEUE_LENGTH 30

//denotes that the following chars will be an unsigned int, higher bits first
#define SERIAL_CODE_UNSIGNED_INT 1

#define TRUE 1
#define FALSE 0

#define INC(bufPtr, buffer) do {bufPtr++; \
    if(bufPtr >= buffer + BUFFER_LENGTH) bufPtr = buffer; \
    } while(0)

#define DO_LATER(funcName, funcData) do { \
    *workQueue[prodInd] = funcName; \
    *workData[prodInd] = (void *)funcData; \
    INCW(prodInd); \
    } while(0)
}
//increment work queue index, move it back to the start if needed to make the queue ciruclar
#define INCW(index) do {index++; \
    if(index >= WORK_QUEUE_LENGTH) index = 0; \
    } while(0)



#endif // QUEUE_H
