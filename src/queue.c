#include "queue.h"

char toSend;
char outBuffer[BUFFER_LENGTH];
char inBuffer[BUFFER_LENGTH];

char * outBufRd;
char * outBufWt;
char * inBufRd;
char * inBufWt;

unsigned int globalCounter; //FOR TESTING

void main_queue(void) {
    int i;
    init_globals();
    init_LEDs();
    initSCI();

    //REMOVE THESE WHEN NOT TESTING
    init_debug_isr();
    TC7 = TCNT + 24000;
    globalCounter = 0x1000;

    EnableInterrupts;
    // for(i = 0; i < 5; i++)
    //     time_function(time_me, NULL);

    while (1) {
        if(prodInd != workerInd) { //if there is work to be done
            (*workQueue[workerInd])(workData[workerInd]);
            INCW(workerInd);
        }
  	}
}

void do_input_command(void * ptr) {
 unsigned int i;
    char lcdMsg[32];
    char * msgPtr;
    char * chPtr;
    char chr;
  //d<message>_ display message on LCD
  if(*inBufRd == 'd') {
      //inBufRd++; //move past 'd'
      INC(inBufRd, inBuffer);
      for(i = 0; i < 32; i++) lcdMsg[i] = 0; //zero out the buffer so the last message doesn't show
      msgPtr = lcdMsg;
      while(*inBufRd != '_') { //note that there is no range check
          *msgPtr++ = *inBufRd; //NEED TO MAKE CIRCULAR
          INC(inBufRd, inBuffer);
      }
      //inBufRd++; //move past underscore
      INC(inBufRd, inBuffer);
      LCD_put_string(lcdMsg);
  }

  if(*inBufRd == 'e') { //e<message>_ echo message back over serial
      INC(inBufRd, inBuffer);
      chPtr = inBufRd; //point to first character in string

      //replace the _ in the inbuffer with a null terminator, so that a pointer to the inBuffer can be used as an argument. Note that this could cause problems if data is getting input too fast, as the inbuffer could be overwritten too quickly.
      while(*inBufRd != '_') INC(inBufRd, inBuffer);
      *inBufRd = 0;
      INC(inBufRd, inBuffer); //move past null terminator
      serial_tx_str(chPtr);
  }

  if(*inBufRd == 'l') {//lHH_ - Set the leds to 0xHH (must be capital letters)
      chr = 0;
      INC(inBufRd, inBuffer);
      if(*inBufRd >= '0' && *inBufRd <= '9') chr += 16 * (*inBufRd - '0');
      else chr += 16 * (*inBufRd - 'A' + 10);
      INC(inBufRd, inBuffer);
      if(*inBufRd >= '0' && *inBufRd <= '9') chr += (*inBufRd - '0');
      else chr += (*inBufRd - 'A' + 10);
      PORTB = chr;
      INC(inBufRd, inBuffer);
      INC(inBufRd, inBuffer);
  }
  if(*inBufRd == 'g') { //g_ send back the global counter as an raw int
      serial_send_uint(globalCounter);
      INC(inBufRd, inBuffer);
      INC(inBufRd, inBuffer);
  }
}

void init_globals() {
    outBufWt = outBuffer;
    outBufRd = outBuffer;
    inBufRd = inBuffer;
    inBufWt = inBuffer;
    prodInd = 0;
    workerInd = 0;
}

void init_LEDs() {
    DDRB= 0xFF;   /* Port B output */
    DDRJ= 0xFF;   // Port J to Output
    PTJ = 0x00;   // enable LEDs
    PORTB=0xFF;     // debuging info
}

void inc_leds(void * ptr) {
    PORTB = PORTB + 1;
}

/*
//USES SAME TIMER AS IIC, Prescaler at maximum
//Generate a timer interrupt as infrequently as possible
interrupt 15 void debug_timer_isr(void) {

    TC7 =TCNT - 1;
    TFLG1=TFLG1 | TFLG1_C7F_MASK;
    //globalCounter++;
    // *workQueue[prodInd] = inc_leds;
    // INCW(prodInd);
    DO_LATER(inc_leds, NULL);
}
*/
