//Displaying "HELLO" on LCD for Dragon12+ Trainer Board
//with HCS12 Serial Monitor Program installed. This code is for CodeWarrior IDE
//On Dragon12+ LCD data pins of D7-D4 are connected to Pk5-Pk2, En=Pk1,and RS=Pk0,
////Modified from Mazidi's book, Eduardo nebot  28/3/15
// included a loop to write some text
// need to get proper timing and make it more usable
//  COMWRT4(0x80) is comand for first line first character
// COMWRT4(0xC0)   is comand for second line first character
// need to evaluate proper delay

#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "lcd.h"


#define LCD_DATA PORTK
#define LCD_CTRL PORTK
#define RS 0x01
#define EN 0x02

//http://www.ee.nmt.edu/~rison/ee308_spr12/notes/Lecture_24.pdf
//details on how to optimise. It looks like the timing can be greatly reduced, and not all commands are necessary.


void LCD_init() {
    DDRK = 0xFF;
    COMWRT4(0x33);   //reset sequence provided by data sheet
    MSDelay(1);
    COMWRT4(0x32);   //reset sequence provided by data sheet
    MSDelay(1);
    COMWRT4(0x28);   //Function set to four bit data length
                                     //2 line, 5 x 7 dot format
    MSDelay(1);
    COMWRT4(0x06);  //entry mode set, increment, no shift
    MSDelay(1);
    COMWRT4(0x0E);  //Display set, disp on, cursor on, blink off
    MSDelay(1);
    COMWRT4(0x01);  //Clear display
    MSDelay(1);
    COMWRT4(0x80);  //set start posistion, home position
    MSDelay(1);
}

void LCD_put_string(char * string) {
    int i;
    LCD_init();
    for(i = 0; i < 16; i++) {
        if(!string[i]) return;
        DATWRT4((unsigned char)string[i]);
    }
    COMWRT4(0xC0);  //set start posistion, home position
    MSDelay(1);
    for(i = 16; i < 32; i++) {
      if(!string[i]) return;
      DATWRT4((unsigned char)string[i]);
    }
}


void COMWRT4(unsigned char command)  {
    unsigned char x;

    x = (command & 0xF0) >> 2;         //shift high nibble to center of byte for Pk5-Pk2
    LCD_DATA =LCD_DATA & ~0x3C;          //clear bits Pk5-Pk2
    LCD_DATA = LCD_DATA | x;          //sends high nibble to PORTK
    MSDelay(1);
    LCD_CTRL = LCD_CTRL & ~RS;         //set RS to command (RS=0)
    MSDelay(1);
    LCD_CTRL = LCD_CTRL | EN;          //rais enable
    MSDelay(5);
    LCD_CTRL = LCD_CTRL & ~EN;         //Drop enable to capture command
    MSDelay(15);                       //wait
    x = (command & 0x0F)<< 2;          // shift low nibble to center of byte for Pk5-Pk2
    LCD_DATA =LCD_DATA & ~0x3C;         //clear bits Pk5-Pk2
    LCD_DATA =LCD_DATA | x;             //send low nibble to PORTK
    LCD_CTRL = LCD_CTRL | EN;          //rais enable
    MSDelay(5);
    LCD_CTRL = LCD_CTRL & ~EN;         //drop enable to capture command
    MSDelay(15);
  }

void DATWRT4(unsigned char data)  {
    unsigned char x;
    x = (data & 0xF0) >> 2;
    LCD_DATA =LCD_DATA & ~0x3C;
    LCD_DATA = LCD_DATA | x;
    MSDelay(1);
    LCD_CTRL = LCD_CTRL | RS;
    MSDelay(1);
    LCD_CTRL = LCD_CTRL | EN;
    MSDelay(1);
    LCD_CTRL = LCD_CTRL & ~EN;
    MSDelay(5);

    x = (data & 0x0F)<< 2;
    LCD_DATA =LCD_DATA & ~0x3C;
    LCD_DATA = LCD_DATA | x;
    LCD_CTRL = LCD_CTRL | EN;
    MSDelay(1);
    LCD_CTRL = LCD_CTRL & ~EN;
    MSDelay(15);
  }


 void MSDelay(unsigned int itime)
  {
    unsigned int i; unsigned int j;
    for(i=0;i<itime;i++)
     for(j=0;j<1000;j++);
 }
