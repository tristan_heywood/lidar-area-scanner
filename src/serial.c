
char commandReady;
/* Initialises the registers associated for the serial communications
   interface. A 9600 baud rate is used, RDRF interrupts are enabled,
   transmitter and receiver enabled. */
void initSCI(void) {
  SCI1BDH = 0x0;
  SCI1BDL = 0x9C; // 9600 Baud Rate
  SCI1CR1 = 0x0;
  SCI1CR2 = 0x2C; // Enable interrupts on RDRF, Receiver, Transmitter
}
void serial_tx_str(char * str) {
    while(*str != 0) {
        *outBufWt = *str++;
        INC(outBufWt, outBuffer);
    }
    *outBufWt = '_';
    INC(outBufWt, outBuffer);
    SCI1CR2 |= 0x80; //todo : not sure how serial works, is this necessary to get transmit interrupts?
}

void serial_send_uint(unsigned int i) {
    *outBufWt = SERIAL_CODE_UNSIGNED_INT;
    INC(outBufWt, outBuffer);
    *outBufWt = i >> 8; //get higher bits
    INC(outBufWt, outBuffer);
    *outBufWt = i & 0x00FF; //get lower bits
    INC(outBufWt, outBuffer);
    *outBufWt = '_';
    INC(outBufWt, outBuffer);
    SCI1CR2 |= 0x80;
}

/* Interrupt Service Routine initiated by the SCI1 interrupt */
interrupt 21 void SCI1_ISR(void) {
    char in;
    if (SCI1SR1 & SCI1SR1_RDRF_MASK) { // If this is an receiver (RDRF) interrupt,
      in = SCI1DRL & 0b01111111; //dodgy work around because python seems to send the wrong thing sometimes
      *inBufWt = in;
      INC(inBufWt, inBuffer);

      if(in == '_') {
          workQueue[prodInd] = do_input_command;
          INCW(prodInd);
      }
      SCI1CR2 |= 0x80;                 //   interrupt, to start/continue sending data
    }

    if (SCI1SR1 & SCI1SR1_TDRE_MASK) { // If this is a ready-to-transmit (TDRE) interrupt,
        if(outBufRd != outBufWt) {
            SCI1DRL = *outBufRd;
            INC(outBufRd, outBuffer);
        }
        else {
            SCI1CR2 &= 0x7F; //stop receiving transmist interrupts
        }
    }
}
