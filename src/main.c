<<<<<<< HEAD
=======
#include "derivative.h"
#include "servo.h"
#include "scan.h"


void Timer_setup(void);
void Calc_distance(void);


void main(void)
{
    unsigned int rising_edge; //Both unsigned int's as they're 16 bits.
    unsigned int falling_edge;
    unsigned int pulse_width ; //I think these would eventually be in the main.
    unsigned int distance ;

    Timer_setup();
    initServo();
    
    setPanAngle(-5000);
    setTiltAngle(2000);
    
    setMode(FAST);
    
    while(1)
    {
    
    //Just calculate the distance.
    //Check in code warrior the value of distance when the code is paused. 
    }


}



void Timer_setup(void)
{
    _asm CLI;      //Enable interrupts

    TSCR1 = 0x90 ; //Enable fast flag clear and normal timer operation
    TSCR2 = 0x04 ; //Set the prescaler to 16.
    TIOS  = 0x00 ; //Enable input capture on Port T pin 1 (as specified)
    TCTL3 = 0x00 ; //Want to capture time signal goes high and drops back.
    TCTL4 = 0x04 ; // Rising edge on pin 1.
    TIE  |= 0x02 ; //Enable interrupts from pin 1.

}


/*****************************************************************************/
/* Part 4, ISR referring to Timer channel 1 */
/* This interrupt is generated when the wave pulls high, or low. */
/* Have decided to user Timer 1.*/

unsigned int rising_edge; //Both unsigned int's as they're 16 bits.
unsigned int falling_edge;
unsigned int pulse_width ; //I think these would eventually be in the main.
unsigned int distance ;

interrupt 9 void Laser_PWM_isr(void)
{
  if(TCTL4 & 0x04) //Bitwise and operation checking if capture rising or falling edge.
  {
      rising_edge = TC1 ; //If a rising edge is detected, store its time stamp
      TCTL4 = 0x08 ; //Set the next interrupt to capture a falling edge.
  }

  else
  {
    falling_edge = TC1 ; //If a falling edge is detected, store its timestamp.
    TCTL4 = 0x04 ; //Set the next interrupt to capture a rising edge.
    Calc_distance();
  }
}
/*****************************************************************************/
/*Part 5: Maths relating to finding distance */
void Calc_distance(void)
{
  pulse_width = falling_edge - rising_edge ; //Not accounting for prescaler or time
  distance = (2*(pulse_width / 3)) ; // TO get the distance measured in mm.

  nextScan(distance);

  //PORTB = distance ; //Display distance on the 7 seg.
    //24 000 000 / 16 = 1 500 000 (prescaler)
    //1 500 000 / 1000msec  = 1500 (A value of pulse width = 1500 means 1m away)
    //1500 / 1.5 = measurement returned will be in millimeters.

    //1 msec = 1 meter
    //0.001s = 1m


}



/*****************************************************************************/
>>>>>>> a104403c9f78cedcd6b90203d2108283693158b3
