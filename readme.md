# LIDAR AREA SCANNER #

## Purpose ##

This project uses a sensor head consisting of two servo motors, a 10 axis intertial sensor and a LIDAR connected to a hcs12 microcontroller. The aim is to use the LIDAR to build a point cloud of an object placed in front of the sensor head, and use this to determine the area of the object, which could potentially have a hole in it. 

## Project Structure ##

The program that runs on the microcontroller is in src/FloatMain/Sources. This program is written in C and is interrupt driven. The program revolves around a queue of function pointers which are continuously called by the main loop. This allows interrupts to scedule tasks to be carried out later and thus prevents interrupts from taking too long. 

The microcontroller is connected to a computer via USB. A python client running on the computer recieves data points from the microcontroller and plots them in real time in a GUI window. This same GUI is also able to configure the program running on the microconroller over USB. The python programs are located in src.